/*
 *	Owned by LIS HQ (C) Copyright
 * 				  2024
 *		  ALL RIGHT RESERVED
 * 
 * 
 * Watch out @Copyright U BASTARD
 *				  _____
 *				 /     \
 *				| () () |
 *				 \  ^  /
 *				  |||||
 *				  |||||
 */

package hq.lis.Sly.spoofers;

import hq.lis.Sly.backpack.PDFDocument;
import hq.lis.Sly.backpack.Page;
import hq.lis.Sly.backpack.Page.PageExtractor;
import hq.lis.Sly.danger.BendSymbol;

public final class ClassicConverter implements Spoofer {
	private class Converter {
		
	}

	private PDFDocument file;

	public void convertPages(int from, int to, boolean isBackw) {
		int pair[] = { from, to };
		
		try {
			PageExtractor.parse(pair, file.getPageAmount(), isBackw);
		} catch (BendSymbol bendSymbol) {
			bendSymbol.printStackTrace();
		}
	}

	public void convertPages(PageExtractor pageExtractor, int unions[][], boolean isBackw) {
		 // #TODO: to choose method to handle exception
		
		for (int union[] : unions) try {
				pageExtractor.parse(union, file.getPageAmount(), false);
				
			} catch (BendSymbol bendSymbol) {
				bendSymbol.printStackTrace();
		}
	}

	public void convertAllPages() {

	}

	ClassicConverter() {

	}

}

/* ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⣀⣀⡀⢀⣶⣿⠃⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣤⣶⣿⣿⣿⣿⣿⣿⣿⣿⣇⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡆⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⣾⣿⣿⣿⣿⣿⣟⠙⢿⣿⣿⣿⣿⣿⣿⣷⡄⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⣿⣷⣄⠻⣿⣿⣿⣿⣿⣿⣿⣄⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⢰⣿⣿⣿⣿⣿⣿⣿⣿⣿⠃⠀⠈⠻⠿⠿⣿⣿⣿⣿⣆⠀⠀⠀⠀
⠀⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄⠀⠀⠀⠀⠀⠀⠙⢿⣿⠟⠀⠀⠀⠀
⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠦⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⢰⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀*/