/*
 *	Owned by LIS HQ (C) Copyright
 * 				  2024
 *		  ALL RIGHT RESERVED
 * 
 * 
 * Watch out @Copyright anno U BASTARD
 *				  _____
 *				 /     \
 *				| () () |
 *				 \  ^  /
 *				  |||||
 *				  |||||
 * 
 * 
 * This is a main class. The software Slyyy is intended to view and convert PDF file to
 * different formats and back, as well as freely modifying their content/media.
 * 
 * There are 4 main components:
 * - backpack (contains resource classes representing documents and their parts; icons, media)
 * - danger (classes which extends Exception, Throwable)
 * - renderer (renders files, pages and content)
 * - spoofers (factories of converters which brings this app to actions)
 * - localizer (provides international capabilities to this appp)
 * - nastyparser (NastyParser comes into play)
 * - flyer (adds some action services to the app)
 * 
 * The term "Spoofer" refers to tool which can create an edited pdf file;
 * by the term "spoofing PDF" it is meant to hack file's nasty structure since any acceptable
 * human tool has been found to get job done
 * 
 * 
 * Features available in this version:
 * - Converting pages to images and back
 * 
 * 
 * 		Argument HELP (POV u r using desktop version)
 * Usage:
 * 		java Sly [-t], [-i], [-f <path> -m <mode_id> [... , -o <path>]]
 * 
 * -t/--threads - sets thread count for files to be processed in parallel
 * -i/--interactive - runs app interactive cli way
 * -f/--file <path> - takes path of file to get job done w/ it
 * -m/--mode <mode_id> - sets mode in which app operates
 * -v/--version - prints version and goes on running or quits
 * -u/--update - checks for update
 * -p/--pages - specifies the page order to be converted
 * -r/--raccoon - never use it
 */

package hq.lis.Sly;

import java.awt.Container;
import java.lang.reflect.Field;

import javax.swing.JPanel;

import org.junit.jupiter.params.shadow.com.univocity.parsers.annotations.Copy;

import hq.lis.Sly.nastyparser.NastyParser;
import hq.lis.Sly.spoofers.Spoofer;

public class Slyyy {
	public static void main(String args[]) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException {
		// Class.forName("hq.lis.Sly.Raccoon");
		/*NastyParser nasty = new NastyParser(args);
		nasty.addArg("-f", "--file", true);
		nasty.addArg("-p", "--pages", true);
		nasty.addArg("-o", "--output", true);
		nasty.addCritical("-i", "--interactive", false);
		
		
		
		Spoofer pdfToImages = SpoofingFactory.buildPDFSpoofer(filename, toSerialize, needsBackwards);
		pdfToImages.convertPagesToImages();*/
		
	}

}

/* ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⣀⣀⡀⢀⣶⣿⠃⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣤⣶⣿⣿⣿⣿⣿⣿⣿⣿⣇⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡆⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⣾⣿⣿⣿⣿⣿⣟⠙⢿⣿⣿⣿⣿⣿⣿⣷⡄⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⣿⣷⣄⠻⣿⣿⣿⣿⣿⣿⣿⣄⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⢰⣿⣿⣿⣿⣿⣿⣿⣿⣿⠃⠀⠈⠻⠿⠿⣿⣿⣿⣿⣆⠀⠀⠀⠀
⠀⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄⠀⠀⠀⠀⠀⠀⠙⢿⣿⠟⠀⠀⠀⠀
⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠦⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⢰⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀*/
