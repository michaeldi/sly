/*
 *	Owned by LIS HQ (C) Copyright
 * 				  2024
 *		  ALL RIGHT RESERVED
 */

package hq.lis.Sly.nastyparser;

import java.util.HashMap;

public class NastyParser {
	private boolean isParsed;
	private HashMap<String, String> map;
	private String args[];
	
	public void addArg(String arg, boolean waitsFor) {
		map.put(arg, null);
	}
	
	public void addArg(String arg, String altArg, boolean waitsFor) {
		map.put(arg, null);
		map.put(altArg, null);
	}
	
	public void addArg(boolean waitFor, String ... args) {
		for(String arg : args) {
			map.put(arg, null);
		}
	}
	
	public void addCritical(String arg, boolean waitsFor) {}
	
	public void addCritical(String arg, String altArg, boolean waitsFor) {}
	
	public void addCritical(boolean waitFor, String ... args) {}
	
	public String getValue(String arg) {
		return "";
	}
	
	public void parseArgs() {
		isParsed = true;
		
	}
	
	public NastyParser(ARG_LIST arglist, String args[]) {
		
	}
	
	public NastyParser(String args[]) {
		this.args = args;
	}
	
	

}

/* ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⣀⣀⡀⢀⣶⣿⠃⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣤⣶⣿⣿⣿⣿⣿⣿⣿⣿⣇⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡆⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⣾⣿⣿⣿⣿⣿⣟⠙⢿⣿⣿⣿⣿⣿⣿⣷⡄⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⣿⣷⣄⠻⣿⣿⣿⣿⣿⣿⣿⣄⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⢰⣿⣿⣿⣿⣿⣿⣿⣿⣿⠃⠀⠈⠻⠿⠿⣿⣿⣿⣿⣆⠀⠀⠀⠀
⠀⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄⠀⠀⠀⠀⠀⠀⠙⢿⣿⠟⠀⠀⠀⠀
⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠦⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⢰⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀*/