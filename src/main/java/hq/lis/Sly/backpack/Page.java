/*
 *	Owned by LIS HQ (C) Copyright
 * 				  2024
 *		  ALL RIGHT RESERVED
 * 
 * 
 * Watch out @Copyright U BASTARD
 *				  _____
 *				 /     \
 *				| () () |
 *				 \  ^  /
 *				  |||||
 *				  |||||
 *
 *
 * 
 */

package hq.lis.Sly.backpack;

import java.util.ArrayList;

import hq.lis.Sly.backpack.media.*;
import hq.lis.Sly.danger.BendSymbol;

public abstract class Page {
	public static class PageExtractor {
		private int firstPair[];
		private int currentPair[];
		private boolean backw;
		
		
		public void setBackwards(boolean backw) {
			this.backw = backw;
		}
		
		
		public boolean getBackwards() {
			return backw;
		}
		
		
		public static void parse(int union[], int pageCount, boolean backw) throws BendSymbol {
			if(union.length != 2)
				throw new BendSymbol(""); // #TODO: throw with message from BSMess
			
			if(!backw && union[1] < union[0]) {
				throw new BendSymbol("");
				
			} else if(backw && union[1] > union[0]) {
				throw new BendSymbol("");
			}

		}
		
		
		@Deprecated(since="the time it was created")
		public static boolean correctPair(int union[], int pageCount, boolean backw) {
			boolean isCorrect = false;
			try {
				parse(union, pageCount, backw);
				isCorrect = true;
				
			} catch(BendSymbol bendSymbol) {}
			
			return isCorrect;
		}
		
		
		public void parsePair(int union[], boolean backw) {
			currentPair = union;
		}
		
		
		public PageExtractor(int unions[][], PDFDocument file, boolean backw) throws BendSymbol {
			this(unions, file);
			this.backw = backw;
		}
		
		
		// this is main constructor
		public PageExtractor(int unions[][], PDFDocument file) throws BendSymbol {
			if(unions.length != 0) {
				parse(unions[0], file.getPageAmount(), backw);
				firstPair = unions[0];
				
			} else
				throw new BendSymbol("");
		}
	}
	
	
	private Layout layout;
	private ArrayList<Resource> res;

	
	protected Page(Layout layout, ArrayList<Resource> resource) {
		
	}
	
}

/* ⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣀⣀⣀⡀⢀⣶⣿⠃⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⠀⢀⣤⣶⣿⣿⣿⣿⣿⣿⣿⣿⣇⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⠀⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⡆⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⠀⣾⣿⣿⣿⣿⣿⣟⠙⢿⣿⣿⣿⣿⣿⣿⣷⡄⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⣿⣷⣄⠻⣿⣿⣿⣿⣿⣿⣿⣄⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⢰⣿⣿⣿⣿⣿⣿⣿⣿⣿⠃⠀⠈⠻⠿⠿⣿⣿⣿⣿⣆⠀⠀⠀⠀
⠀⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⣿⣿⣿⡄⠀⠀⠀⠀⠀⠀⠙⢿⣿⠟⠀⠀⠀⠀
⠀⠀⠀⠀⠀⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠿⠦⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⢰⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⣶⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⠀⢸⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠀⠀*/